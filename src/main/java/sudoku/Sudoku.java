
package org.bitbucket.sudoku;

/**
 * Sudoku
 */
public class Sudoku {

  /**
   * The n x m grid that defines the Sudoku
   */
  private int[][] theGrid;

  /**
   * Constructor for Sudoku
   *
   * @param g   The grid that defines the sudoku
   */
  public Sudoku(int[][] g) {
    theGrid = g;
  }

  /**
   *  Check validity of a Sudoku grid
   *
   *  @return true if and only if theGrid is a valid Sudoku
   */
  public boolean isValid()  {
    //  check size of grid
    if (theGrid.length == 0)
      return true;
    else {

      //  Grid is not empty
      // Check that the lengths of subarrays are equal to base array
      // (to see if grid height and width are equal)
      for(int i = 0; i<theGrid.length; i++){
        if(theGrid[i].length != theGrid.length) {
          // Grid is not a square
          return false;
        }

        // Check all values in row are legal
        for(int j = 0; j<theGrid[i].length; j++) {
          if(!(theGrid[i][j] >= 1 && theGrid[i][j] <= theGrid.length)){
            // Value out of bounds
            return false;
          }
        }
      }

      // Now we know the grid is square and has all legal values,
      // Now check if grid size is square number
      if(!isPerfectSquare(theGrid.length)) {
        return false;
      }

      // Grid passes all checks, is valid grid
      return true;
    }
  }

  /**
   * Checks if number is a perfect square
   *
   * Source: https://stackoverflow.com/questions/343852/whats-a-good-algorithm-to-determine-if-an-input-is-a-perfect-square
   **/
   boolean isPerfectSquare(long input)
  {
    long closestRoot = (long) Math.sqrt(input);
    return input == closestRoot * closestRoot;
  }
}
